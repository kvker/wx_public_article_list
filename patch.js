// ==UserScript==
// @name         微信公众号内其他公众号文章链接采集
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  spider it!
// @author       kvker
// @match        https://mp.weixin.qq.com/cgi-bin/appmsg
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  // Your code here...

  let timeout = 2000

  // 链接列表
  let list = []
  // 如果有特殊情况, 可以使用这个字段访问现在进度, 比如被爬虫挂了等等
  window.patach_list = list

  // 进度
  let page_progress = '<progress style="position: fixed; top: 80px; right: 120px; z-index: 9999;background-color: white;" id="page_progress"></progress>'

  // 开始按钮
  let start_btn = `
    <button onclick="patchListPluginStart()" style="position: fixed; top: 120px; right: 120px; z-index: 9999;background-color: white; padding: 8px 16px">开始</button>
`
  // 结束文本框, 保存数据
  let end_ele = `
    <textarea id="result_text_area" style="position: fixed; top: 200px; right: 120px; z-index: 9999;background-color: white; padding: 8px 16px" placeholder="结果展示区"></textarea>
`
  document.body.insertAdjacentHTML('afterend', start_btn)
  document.body.insertAdjacentHTML('afterend', page_progress)
  document.body.insertAdjacentHTML('afterend', end_ele)


  // 页码进度实例
  let page_progress_ins = document.querySelector('#page_progress')
  // 结果展示实例
  let result_text_area_ins = document.querySelector('#result_text_area')

  window.patchListPluginStart = function start() {
    console.log('start')
    patch()
  }

  window.patchListPluginEnd = function end() {
    if(!document.querySelector('#result_text_area')) {
      document.body.insertAdjacentHTML('afterend', end_ele)
    }
    result_text_area_ins.value = JSON.stringify(list)
    console.log('end list length: ' + list.length)
  }

  function patch() {
    // 下一页按钮
    let next_ctrl = getNextPageCtrl()
    // 获取a标签
    let as = getAs()
    // 获取标题, 可选
    let titles = getTitles()
    as.forEach(function(a, idx) {
      // 除链接外其他处理 start
      if(titles[idx].innerText.includes('壁纸')) {
        list.push(a.href)
      }
      // end
    })
    console.log('list length: ' + list.length)

    // 有页码则翻页
    if(document.querySelector('.weui-desktop-pagination__num')) {
      let current_page = $('.weui-desktop-pagination__num')[0].innerText
      let max_page = $('.weui-desktop-pagination__num')[1].innerText
      page_progress_ins.value = current_page
      page_progress_ins.max = max_page
      console.log(`progress: ${current_page}/${max_page}`)

      // 29的倍数则50秒延迟, 否则反爬虫太凶
      if(!(current_page % 29)) {
        console.log('进入休息反爬虫, 时长为: ' + 50000 + '毫秒')
        timeout = 50000
      } else {
        timeout = 2000
      }

      // 当前页码等于最大页码则结束
      if(current_page === max_page) {
        patchListPluginEnd()
        return
      } else {
        // 点击下一页事件
        if(next_ctrl) {
          next_ctrl.click()
          // 延时2s后开始采集并插入列表
          setTimeout(function() {
            patch()
          }, timeout)
        }
      }
    } else { // 没页码直接结束
      patchListPluginEnd()
      return
    }
  }

  // 获取下一页按钮
  function getNextPageCtrl() {
    let as = document.querySelectorAll('.weui-desktop-btn.weui-desktop-btn_default.weui-desktop-btn_mini')
    // 第一页只有一个下一页
    return as[1] || as[0]
  }

  // 获取当前列表的a标签
  function getAs() {
    return document.querySelectorAll('.weui-desktop-vm_default a')
  }

  // 获取标题
  function getTitles(){
    return document.querySelectorAll('.inner_link_article_title')
  }
})();